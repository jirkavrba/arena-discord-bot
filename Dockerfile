FROM maven:3.9.5-eclipse-temurin-17 as build
WORKDIR /app

COPY pom.xml ./
COPY src ./src

RUN --mount=type=cache,target=~/.m2/repository mvn clean package

FROM eclipse-temurin:17-jdk
WORKDIR /app

COPY --from=build /app/target/*.jar ./app.jar

ENTRYPOINT ["java", "-jar","app.jar", "-v"]
