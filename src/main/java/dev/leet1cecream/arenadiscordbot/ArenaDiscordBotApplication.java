package dev.leet1cecream.arenadiscordbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArenaDiscordBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArenaDiscordBotApplication.class, args);
    }

}
