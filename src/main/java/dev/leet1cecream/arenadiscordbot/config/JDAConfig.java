package dev.leet1cecream.arenadiscordbot.config;

import dev.leet1cecream.arenadiscordbot.listeners.commands.SlashCommand;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

import java.util.List;

@Slf4j
@SpringBootConfiguration
public class JDAConfig {

    List<SlashCommand> slashCommands;
    List<? extends ListenerAdapter> listenerAdapters;

    public JDAConfig(List<SlashCommand> slashCommands, List<? extends ListenerAdapter> listenerAdapters) {
        this.slashCommands = slashCommands;
        this.listenerAdapters = listenerAdapters;
    }

    @Bean
    public JDA jda() throws InterruptedException {
        final String botToken = System.getenv("DISCORD_TOKEN");
        assert(!botToken.isEmpty()) : "Bot token environment variable cannot be null.";

        JDA jda = JDABuilder.createDefault(botToken,
                        GatewayIntent.GUILD_MEMBERS,
                        GatewayIntent.GUILD_MESSAGES,
                        GatewayIntent.MESSAGE_CONTENT)
                .setActivity(Activity.watching("you"))
                .build();
        jda.awaitReady();

        // Register event listeners
        for (ListenerAdapter listenerAdapter : listenerAdapters) {
            jda.addEventListener(listenerAdapter);
        }

        // Register commands with each guild
        for (Guild guild : jda.getGuilds()) {
            guild.updateCommands().addCommands(
                    slashCommands.stream()
                            .map(SlashCommand::getSlashCommandData)
                            .toList()
            ).queue();
        }


        return jda;
    }
}
