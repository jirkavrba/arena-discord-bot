package dev.leet1cecream.arenadiscordbot.listeners;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.emoji.Emoji;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.MessageUpdateEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.stereotype.Component;

@Component
public class CoalMineCoalReactListener extends ListenerAdapter {

    public static final long COAL_MINE_THREAD_ID = 1157371720601174068L;
    public static final String COAL_EMOJI_ID = "<:uhli:1099282427626651758>";

    boolean shouldReactWithCoal(Message message) {
        // Check if message contains either image or video attachment
        for (Message.Attachment attachment : message.getAttachments()) {
            if (attachment.isImage() || attachment.isVideo()) {
                return true;
            }
        }

        // Check for specific text in embed title
        for (MessageEmbed embed : message.getEmbeds()) {
            String title = embed.getTitle();
            if (title != null && title.startsWith("Tady máš gem")) {
                return true;
            }
        }

        return false;
    }

    boolean isMessageFromCoalMine(Message message) {
        return message.getChannel().getIdLong() == COAL_MINE_THREAD_ID;
    }

    void processMessage(Message message) {
        if (isMessageFromCoalMine(message) && shouldReactWithCoal(message)) {
            message.addReaction(Emoji.fromFormatted(COAL_EMOJI_ID)).queue();
        }
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        processMessage(event.getMessage());
    }

    @Override
    public void onMessageUpdate(MessageUpdateEvent event) {
        processMessage(event.getMessage());
    }
}
