package dev.leet1cecream.arenadiscordbot.listeners.commands;

import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@Slf4j
@Component
public class GetCoalSlashCommand extends ListenerAdapter implements SlashCommand {

    public static final String COMMAND_NAME = "get-coal";
    public static final String COMMAND_DESCRIPTION = "Returns a random image from Lajtkek's LightGallery";
    private static final String URL_LIGHTGALLERY_RANDOM_IMAGE = "https://gallery.lajtkep.dev/api/files/getRandomFile.php?seed=%s";

    @Override
    public SlashCommandData getSlashCommandData() {
        return Commands.slash(COMMAND_NAME, COMMAND_DESCRIPTION);
    }

    @Override
    public void onSlashCommandInteraction(@NotNull SlashCommandInteractionEvent event) {
        if (event.getName().equals(COMMAND_NAME)) {
            event.deferReply().queue();
            if (testGalleryConnection()) {
                long currentTimestamp = System.currentTimeMillis() / 1000L;
                String username = event.getInteraction().getUser().getName();
                event.getHook().sendMessage("").addEmbeds(
                        new EmbedBuilder()
                                .setTitle("Here is your coal %s".formatted(username))
                                .setImage(URL_LIGHTGALLERY_RANDOM_IMAGE.formatted(currentTimestamp))
                                .build()
                ).queue();
            }
            else {
                event.getHook().sendMessage("LightGallery je down <:xd:1067222044141424680>").queue();
            }
        }
    }

    private static boolean testGalleryConnection() {
        try {
            URL url = new URL("https://gallery.lajtkep.dev/api/files/getRandomFile.php");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("HEAD");
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            return con.getResponseCode() == 200;
        } catch (MalformedURLException e) {
            log.error("Malformed URL | {}", e.getMessage());
        } catch (IOException e) {
            log.error("IOException | {}", e.getMessage());
        }
        return false;
    }
}
