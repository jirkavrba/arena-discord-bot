package dev.leet1cecream.arenadiscordbot.listeners.commands;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.json.JsonMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.Command;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
public class GetRandomQuoteSlashCommand extends ListenerAdapter implements SlashCommand {

    public static final String COMMAND_NAME = "get-random-quote";
    public static final String COMMAND_DESCRIPTION = "Provides a radomly selected quote.";

    JsonMapper jsonMapper;
    List<Quote> quotes = new ArrayList<>();

    public GetRandomQuoteSlashCommand(JsonMapper jsonMapper) {
        this.jsonMapper = jsonMapper;
        ClassPathResource classPathQuotes = new ClassPathResource("json/quotes.json");
        try {
            quotes = jsonMapper.readValue(classPathQuotes.getInputStream(), new TypeReference<>() {});
        } catch (IOException e) {
            log.error("Failed to load quotes.json file | {} | {}", e, e.getMessage());
        }
    }

    @Getter @AllArgsConstructor @NoArgsConstructor
    private static class Quote {
        String quote;
        String author;
    }

    @Override
    public SlashCommandData getSlashCommandData() {
        return Commands.slash(COMMAND_NAME, COMMAND_DESCRIPTION)
                .addOptions(
                        new OptionData(OptionType.STRING, "author", "The author of the random quote")
                                .addChoices(mapAuthorsToChoices(getAuthorsSet()))
                );
    }

    @Override
    public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
        if (!event.getName().equals(COMMAND_NAME)) {
            return;
        }
        event.deferReply().queue();

        if (quotes.isEmpty()) {
            sendQuote(event, getDefaultQuote());
            return;
        }

        OptionMapping authorOption = event.getOption("author");
        if (authorOption != null) {
            String author = authorOption.getAsString();
            List<Quote> filteredQuotes = quotes.stream().filter(quote -> quote.getAuthor().equals(author)).toList();
            sendRandomQuote(event, filteredQuotes);
        }
        else {
            sendRandomQuote(event, quotes);
        }
    }

    private Set<String> getAuthorsSet() {
        return quotes.stream()
                .map(Quote::getAuthor)
                .collect(Collectors.toSet());
    }

    private Set<Command.Choice> mapAuthorsToChoices(Set<String> authors) {
        return authors.stream()
                .map(author -> new Command.Choice(author, author))
                .collect(Collectors.toSet());
    }

    MessageEmbed quoteToEmbed(Quote quote) {
        return new EmbedBuilder()
                .setDescription("### %s\n \\- %s".formatted(quote.getQuote(), quote.getAuthor()))
                .build();
    }

    Quote getDefaultQuote() {
        return new Quote("There are no quotes", "me");
    }

    void sendQuote(SlashCommandInteractionEvent event, Quote quote) {
        event.getHook().sendMessageEmbeds(quoteToEmbed(quote)).queue();
    }

    void sendRandomQuote(SlashCommandInteractionEvent event, List<Quote> quotes) {
        Random random = new Random(System.currentTimeMillis());
        Quote randomQuote = quotes.get(random.nextInt(quotes.size()));
        sendQuote(event, randomQuote);
    }
}
