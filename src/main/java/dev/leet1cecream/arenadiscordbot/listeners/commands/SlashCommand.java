package dev.leet1cecream.arenadiscordbot.listeners.commands;

import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;

public interface SlashCommand {
    SlashCommandData getSlashCommandData();
}
