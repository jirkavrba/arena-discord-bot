package dev.leet1cecream.arenadiscordbot.listeners.commands;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import org.springframework.stereotype.Component;

import java.awt.*;

@Component
public class SourceCodeSlashCommand extends ListenerAdapter implements SlashCommand {

    public static final String COMMAND_NAME = "source-code";
    public static final String COMMAND_DESCRIPTION = "Replies with a link to the bot's source code";

    @Override
    public SlashCommandData getSlashCommandData() {
        return Commands.slash(COMMAND_NAME, COMMAND_DESCRIPTION);
    }

    @Override
    public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
        if (event.getName().equals(COMMAND_NAME)) {
            event.deferReply().queue();
            event.getHook().sendMessageEmbeds(
                    new EmbedBuilder()
                            .setColor(Color.ORANGE)
                            .setAuthor("Ramk0", null, "https://cdn.discordapp.com/avatars/615715441188536361/67134bae67c5f7c809eec9f3a0e9cf36.webp?size=128")
                            .setTitle("arena-discord-bot", "https://gitlab.com/leet1cecream/arena-discord-bot")
                            .setDescription("One of the best bots ever")
                            .build()
            ).queue();
        }
    }
}
